let medDetails = document.getElementById("med-details");
let code = document.getElementById("code");
let name = document.getElementById("name");
let weight = document.getElementById("weight");
let image = document.getElementById("image");
let err = document.getElementById("error");
let cardCont = document.getElementById("card-body");





let medication = JSON.parse(localStorage.getItem("medications")) || [];

let render = () =>{
    cardCont.innerHTML = medication.map((x, y) => {
        return `
     <div class="medication underline p-2" id = ${y}>
     name:<p> ${x.name}</p>
     code:<p > ${x.code}</p>
     weight:<p> ${x.weight}</p>
     image:<p> ${x.image}</p>
     </div>
    `
    }).join("");

};
render();

medDetails.addEventListener('submit', (e) =>{
    e.preventDefault();
    let codeVal = code.value;
    let nameVal = name.value;
    let weightVal = weight.value;
    let imgVal = image.value;


    if( weightVal< 50 || weightVal > 500){
        err.classList.remove("error");
    } else{
        err.classList.add("error");


        let myToken = localStorage.getItem("Token");
        const body = {
            medications:[]

        };
        let medArr = body.medications;
        let bodyArr = {
            name: nameVal,
            weight: weightVal,
            code: codeVal,
            image: imgVal
        };
        medArr.push(bodyArr);

        fetch("https://wazzy-drone-api.herokuapp.com/api/v1/medication", {

            method: "POST",
            headers: {
                "Authorization": `Bearer ${myToken}`,
                "content-type": "application/json",
            },
            body: JSON.stringify(body)
        })
            .then(res => res.json())
            .then(data => {

                if(data.status !== "201 CREATED"){
                    alert(data.message) ;

                }else{
                    medication.push(bodyArr)
                    localStorage.setItem("medications", JSON.stringify(medication));
                    render();
                }
            })
            .catch(err => console.log(err))
        medDetails.reset();


    }



});

logOut = () =>{
    location.href = "index.html";
    localStorage.removeItem("Token");
}




