let cardCont = document.getElementById("card-body");
let userSerial = document.getElementById("userSerialNo");
let loadInp = document.getElementById("loadInp");
let Formbtn = document.getElementById("Formbtn");
let modalForm = document.getElementById("Form1");
let modalCardBody = document.getElementById("modal-card-body")
let userSerial1 = document.getElementById("userSerialNo1");
let Formbtn1 = document.getElementById("Formbtn1");
let modalForm1 = document.getElementById("Form2");
let underline = document.getElementsByClassName("underline");


let myToken = localStorage.getItem("Token");
let loadMedDrone = JSON.parse(localStorage.getItem("loadMed")) ||[];
let drones = JSON.parse(localStorage.getItem("drones")) || [];



let allDrones = () =>{
    let myToken = localStorage.getItem("Token");

    fetch("https://wazzy-drone-api.herokuapp.com/api/v1/drone",{

        headers:{
            "Authorization": `Bearer ${myToken}`,
        }

    })
        .then(res => res.json())
        .then(data => {
            let allDroneArr = data.additionalInfo.drones;

            console.log(allDroneArr)

            cardCont.innerHTML = allDroneArr.map((x, y) => {
                return `
                 <div class="underline p-2" id = ${x.id}>
                  serialNo:<p>${x.serialNo}</p>
                  model:<p class="model">${x.model}</p>
                  state:<p class="state">${x.state}</p>
                  weightLimit:<p>${x.weightLimit}</p>
                  BatteryCapacity:<p>${x.batteryCapacity}</p>
                  createdAt:<p> ${x.createdAt}</p>
                 <button id="loadMedBtn" class="loadMedBtn ${x.state}" onclick="loadMed(this)" data-bs-toggle="modal" data-bs-target="#Form1">load Medication</button>
                 <button onclick="loadedMed(this)" data-bs-toggle="modal" data-bs-target="#Form2">loaded Medication</button>
                 <button onclick="viewBar(this)">Battery Info</button>
                 </div>
    `
            }).join("");


            let loaded = document.querySelectorAll(".loadMedBtn");
            let loadedArr = Array.from(loaded);
            loadedArr.forEach(function (loaded) {
                if (loaded.classList.contains("IDLE")){
                    loaded.style.display = "inline-block";
                }else{
                    loaded.style.display = "none";
                }
            })

        });


};
allDrones();


function sortState(e) {
    let und = document.querySelectorAll(".underline");
    let undArr = Array.from(und);
    undArr.forEach(function (line) {
        let state = line.children.item(2).innerHTML;
        let rmvState = line.children.item(2).classList.contains("state");
       switch (e.value) {
           case "ALL":
               if (e.value === "ALL") {
                   line.classList.remove("rmv-sort")
               }
               break;
           case "IDLE":
               if(e.value === state && state === "IDLE"){
                  line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "LOADING":
               if(e.value === state && state === "LOADING"){
                   line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "LOADED":
               if(e.value === state && state === "LOADED"){
                   line.classList.remove("rmv-sort");

               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "DELIVERING":
               if(e.value === state && state === "DELIVERING"){
                   line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "DELIVERED":
               if(e.value === state && state === "DELIVERED"){
                   line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "RETURNING":
               if(e.value === state && state === "RETURNING"){
                   line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }
               break;

       }

    })

};

function sortModel(e) {
    let und = document.querySelectorAll(".underline");
    let undArr = Array.from(und);
    undArr.forEach(function (line) {
        let model = line.children.item(1).innerHTML;
        let rmvModel = line.children.item(1).classList.contains("model");
        switch (e.value) {
            case "ALL":
                if (e.value === "ALL") {

                    line.classList.remove("rmv-show");

                }
                break;
            case "LIGHTWEIGHT":
                if(e.value === model  && model === "LIGHTWEIGHT"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }

                break;
            case "MIDDLEWEIGHT":
                if(e.value === model && model === "MIDDLEWEIGHT"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }

                break;
            case "CRUISERWEIGHT":
                if(e.value === model && model === "CRUISERWEIGHT"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }

                break;
            case "HEAVYWEIGHT":
                if(e.value === model && model === "HEAVYWEIGHT"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }

                break;
            case "DELIVERED":
                if(e.value === model && model === "DELIVERED"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }
                break;

        }

    })



};

function formReset() {
    let sortModel = document.getElementById("sortModel");
    let sortState = document.getElementById("sortState");
    let und = document.querySelectorAll(".underline");
    let undArr = Array.from(und);
    undArr.forEach(function (line) {
        if (sortState.value === "ALL" && sortModel.value === "ALL") {
            line.classList.remove("rmv-show");
        } else {
            line.classList.add("rmv-show");

        }
    })
};



let viewBar = (e) =>{
    let serialNo = e.parentElement.firstElementChild.innerHTML;
    let myToken = localStorage.getItem("Token");

    fetch(`https://wazzy-drone-api.herokuapp.com/api/v1/drone/${serialNo}/batteryInfo`,{
        method: "GET",
        headers: {
            "Authorization": `Bearer ${myToken}`,
        },
    })
        .then(res => res.json())
        .then(data => {
            alert("battery level is" + " " + data.additionalInfo.battery_level+ "%")
        })

        .catch(err => console.log(err))

};


let loadMed = (e) =>{
    let serialNo = e.parentElement.firstElementChild.innerHTML;
    localStorage.setItem("serialNo", JSON.stringify(serialNo));
    userSerial.innerHTML = JSON.parse(localStorage.getItem("serialNo"))



}

modalForm.addEventListener('submit', (e) => {
    e.preventDefault()

                  let serial = JSON.parse(localStorage.getItem("serialNo"))


                  let body = {
                      medicationCodes:[],
                  }
                  let bodyMedArr = body.medicationCodes;
                  let codes = loadInp.value;
                  console.log(codes)
                  bodyMedArr.push(codes)
                  console.log(bodyMedArr)

                  fetch(`https://wazzy-drone-api.herokuapp.com/api/v1/drone/${serial}/medication`, {

                      method: "PUT",
                      headers: {
                          "Authorization": `Bearer ${myToken}`,
                          "content-type": "application/json",
                      },
                      body: JSON.stringify(body)
                  })
                      .then(res => res.json())
                      .then(data => {

                          if (data.status !== "200 OK"){
                              alert(data.message)

                          }else {
                              alert(data.message)
                              loadMedDrone.push(codes)
                              localStorage.setItem("loadMed", JSON.stringify(loadMedDrone))
                              let search = drones.find((x, y) => x.serialNo === serial);
                              console.log(search)
                              search.state = "LOADED";
                              localStorage.setItem("drones", JSON.stringify(drones));
                              location.reload()
                          }
                      })
                      .catch(err => console.log(err))










    modalForm.reset();

})


let loadedMed = (e) =>{
    let serialNo = e.parentElement.firstElementChild.innerHTML;
    localStorage.setItem("serialNo", JSON.stringify(serialNo));

    let serial = JSON.parse(localStorage.getItem("serialNo"))
    console.log(serial)

    userSerial1.innerHTML = JSON.parse(localStorage.getItem("serialNo"))

    fetch(`https://wazzy-drone-api.herokuapp.com/api/v1/drone/${serialNo}/medication`, {

        headers: {
            "Authorization": `Bearer ${myToken}`,
        },
    })
        .then(res => res.json())
        .then(data => {
            if (data.status !== "200 OK" || data.status  ===  "NOT_FOUND"){

                let errorMsg = data.message;
                let renderModal1 = () =>{
                    modalCardBody.innerHTML = `
       
                               <div class="underline p-2" >
                              <h3>${errorMsg}</h3>     
                               </div>
                            
                            `
                };
                renderModal1();
            }else {
                let loadedDetails = data.additionalInfo.drones[0];


                let renderModal = () =>{
                    modalCardBody.innerHTML = `
       
                               <div class="underline p-2" >
                              <p>Code: ${loadedDetails.code}</p>
                              <p>Name: ${loadedDetails.name}</p>
                              <p>Weight: ${loadedDetails.weight}</p>
                              <p>Image: ${loadedDetails.image}</p>
                            </div>
                            
                            `
                };
                renderModal();
                let loadedDet = {
                    code: loadedDetails.code,
                    name: loadedDetails.name,
                    weight: loadedDetails.weight,
                }

                renderModal();
            }
        })
        .catch(err => console.log(err))

    modalForm.reset();




    // render();
}

logOut = () =>{
    location.href = "index.html";
    localStorage.removeItem("Token");
}
