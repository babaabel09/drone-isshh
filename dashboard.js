let droneDetails = document.getElementById("drone-details");
let serialNo = document.getElementById("serialNo");
let weight = document.getElementById("weight");
let battery = document.getElementById("battery");
let state = document.getElementById("state");
let cardCont = document.getElementById("card-body");
let err = document.getElementById("error");
let err1 = document.getElementById("error1");
let userSerial = document.getElementById("userSerialNo");
let loadInp = document.getElementById("loadInp");
let Formbtn = document.getElementById("Formbtn");
let modalForm = document.getElementById("Form1");
let modalCardBody = document.getElementById("modal-card-body")
let userSerial1 = document.getElementById("userSerialNo1");
let loadInp1 = document.getElementById("loadInp1");
let Formbtn1 = document.getElementById("Formbtn1");
let modalForm1 = document.getElementById("Form2");
 let underline = document.getElementsByClassName("underline");




let drones = JSON.parse(localStorage.getItem("drones")) || [];
let loadMedDrone = JSON.parse(localStorage.getItem("loadMed")) ||[];


let myToken = localStorage.getItem("Token");

let render = () =>{
    cardCont.innerHTML = drones.map((x, y) => {
        return `
     <div class="underline p-2" id = ${y}>
      serialNo:<p>${x.serialNo}</p>
      weightLimit:<p>${x.weightLimit}</p>
      BatteryCapacity:<p>${x.batteryCapacity}</p>
      State:<p class="state"> ${x.state}</p>
   
     </div>
    `
    }).join("");

};
render();





droneDetails.addEventListener('submit', (e) =>{
    e.preventDefault();
    let serial = serialNo.value;
    let weightOfDrone = weight.value;
    let batteryCapacity = battery.value;
    let stateOfDrone = state.value;





    switch ((weightOfDrone < 50 || weightOfDrone > 500) || (batteryCapacity < 0 || batteryCapacity > 100)) {
        case (weightOfDrone < 50 || weightOfDrone > 500) || (batteryCapacity < 0 || batteryCapacity > 100):
            if(weightOfDrone < 50 || weightOfDrone > 500){
                 err.classList.remove("error");
            } else{
                err.classList.add("error");

            }

            if(batteryCapacity < 0 || batteryCapacity > 100){
                err1.classList.remove("error");

            } else{
                err1.classList.add("error");

            }

            if(!((weightOfDrone < 50 || weightOfDrone > 500) || (batteryCapacity < 0 || batteryCapacity > 100))) {

                let myToken = localStorage.getItem("Token");
                const body = {
                            drones:[]

                };
                let droneArr = body.drones;
                let bodyArr = {
                    serialNo: serial,
                    weightLimit: weightOfDrone,
                    batteryCapacity: batteryCapacity,
                    state: stateOfDrone
                };
                 droneArr.push(bodyArr);

                fetch("https://wazzy-drone-api.herokuapp.com/api/v1/drone", {

                    method: "POST",
                    headers: {
                        "Authorization": `Bearer ${myToken}`,
                        "content-type": "application/json",
                    },
                    body: JSON.stringify(body)
                })
                    .then(res => {
                        if(res.status !== 201){

                            if(res.status === 400){
                                alert("serial number exist already");
                            }else{
                                alert("re-enter details again");
                            }
                        }else{

                            return res.json();
                        }
                    })
                    .then(data => {
                        let info = data.additionalInfo.drones;
                        console.log(info[0].createdAt)
                        drones.push(bodyArr)
                        localStorage.setItem("drones", JSON.stringify(drones));
                        render();
                    })
                    .catch(err => console.log(err))
                 droneDetails.reset();
            }
          break;
    }

});







let clearStore = () =>{
    localStorage.clear();
    location.href = "index.html";
}


logOut = () =>{
    location.href = "index.html";
    localStorage.removeItem("Token");
}
