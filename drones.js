let form = document.getElementById("form");
let msg = document.getElementById("msg");
let btn = document.getElementById("btn");
let userName = document.getElementById("username");
let password = document.getElementById("password");
userName.value = "user"
password.value = "password"
let submitted = 0;


msg.style.display = "none";

form.addEventListener('submit', (e) => {
    e.preventDefault();
    if (submitted){
        return;
    }




    msg.style.display = "none";

    const body = {
        username: userName.value,
        password: password.value
    }

    btn.innerHTML = "Loading...";
    submitted = 1;

    const url = "https://wazzy-drone-api.herokuapp.com/auth/v1/token";
    fetch(url, {
        method: "POST",
        headers: {

            "content-type": "application/json",

        },
        body: JSON.stringify(body)

    })
        .then(res => {
            if(res.status !== 200){
                if (res.status === 400){
                    alert("Invalid Credentials")
                }else{
                    alert("An error has occurred")
                }
                btn.innerHTML = "Login";
                submitted = 0;

            }else{
                return res.json()
            }
        })
        .then(data => {
            if(data.access_token){
                let token = data.access_token;
                localStorage.setItem("Token", (token));
                location.href = "./dashboard.html";
            }
        })
        .catch(error => {
            submitted = 0;
            console.log(error)
        })


    form.reset();
});

checkIfLoggedIn = () => {
    const myToken = localStorage.getItem("Token");
    if (myToken) {
        location.href = "./dashboard.html";
    }
};
checkIfLoggedIn();

